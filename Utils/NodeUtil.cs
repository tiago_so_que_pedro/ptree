using UnityEngine;

namespace PTree
{
    public static class NodeUtil
    {
        public static bool UpdatePrintNode(PTreeBehaviourActor actor, PrintNode printNode)
        {
            Debug.Log(printNode.Message);
            CompositeNode compositeNode = GetCompositeNode(actor, printNode.ID);
            actor.BehaviourInfo.NextNode = compositeNode.ChildNode;
            return true;
        }

        public static bool UpdateSelectorNode(PTreeBehaviourActor actor, SelectorNode selectorNode)
        {

            bool completed = false;

            switch (selectorNode.Condition)
            {
                case SelectorCondition.SPOTTED_TARGET:
                    {
                        actor.BehaviourInfo.NextNode = selectorNode.TrueNode;
                        completed = true;
                    }
                    break;
                case SelectorCondition.REACHED_TARGET:
                    {
                        actor.BehaviourInfo.NextNode = selectorNode.FalseNode;
                        completed = true;
                    }
                    break; ;
            }

            return completed;
        }

        public static bool UpdateSequenceNode()
        {
            return false;
        }

        public static Node GetNode(PTreeBehaviourActor actor, int id)
        {
            for (int i = 0; i < actor.BehaviourTree.Nodes.Length; i++)
            {
                var curNode = actor.BehaviourTree.Nodes[i];
                if (curNode.ID == id)
                {
                    return curNode;
                }
            }

            return null;
        }

        public static CompositeNode GetCompositeNode(PTreeBehaviourActor actor, int id)
        {
            for (int i = 0; i < actor.BehaviourTree.CompositeNodes.Length; i++)
            {
                var curNode = actor.BehaviourTree.CompositeNodes[i];
                if (curNode.ID == id)
                {
                    return curNode;
                }
            }

            return null;
        }

        public static PrintNode GetPrintNode(PTreeBehaviourActor actor, int id)
        {
            for (int i = 0; i < actor.BehaviourTree.PrintNodes.Length; i++)
            {
                var curNode = actor.BehaviourTree.PrintNodes[i];
                if (curNode.ID == id)
                    return curNode;
            }

            return null;
        }

        public static SelectorNode GetSelector(PTreeBehaviourActor actor, int id)
        {
            for (int i = 0; i < actor.BehaviourTree.SelectorNodes.Length; i++)
            {
                var curNode = actor.BehaviourTree.SelectorNodes[i];
                if (curNode.ID == id)
                    return curNode;
            }

            return null;
        }
    }
}
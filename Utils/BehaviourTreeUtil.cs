using UnityEngine;

namespace PTree
{
    public static class BehaviourTreeUtil
    {

        public static void InitializeBehaviour(PTreeBehaviourActor actor, BehaviourTreeSO baseBehaviour)
        {
            actor.BehaviourTree = new BehaviourTree();
            actor.BehaviourTree = baseBehaviour.BehaviourTree;
        }

        public static bool UpdateBehaviourNodes(PTreeBehaviourActor actor)
        {
            Node node = NodeUtil.GetNode(actor, actor.BehaviourInfo.RunningNode);
            bool finished = false;

            //If node is null, that means the BehaviourTree reach down in to the end
            if (node == null)
            {
                RestartBehaviour(actor);
                return false;
            }

            //Update SpecificNode case
            switch (node.SpecificNodeType)
            {
                case SpecificNodeType.ROOT:
                    finished = true;
                    actor.BehaviourInfo.NextNode = actor.BehaviourTree.Root.ChildNode;
                    break;
                case SpecificNodeType.PRINT:
                    {
                        PrintNode printNode = NodeUtil.GetPrintNode(actor, node.ID);
                        finished = NodeUtil.UpdatePrintNode(actor, printNode);
                    }
                    break;
                case SpecificNodeType.SELECTOR_NODE:
                    {
                        SelectorNode selectorNode = NodeUtil.GetSelector(actor, node.ID);
                        finished = NodeUtil.UpdateSelectorNode(actor, selectorNode);
                    }
                    break;
            }

            return finished;
        }

        public static void RestartBehaviour(PTreeBehaviourActor actor)
        {
            actor.BehaviourInfo.RunningNode = actor.BehaviourTree.Root.ID;
        }
    }
}
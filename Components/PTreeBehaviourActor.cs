﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTree
{
    public class PTreeBehaviourActor : MonoBehaviour
    {
        public BehaviourTree BehaviourTree;
        [SerializeField]
        private BehaviourTreeSO BehaviourTreeSO;

        public PTreeBehaciourActorInfo BehaviourInfo;

        public void Initialize()
        {
            BehaviourTreeUtil.InitializeBehaviour(this, BehaviourTreeSO);
            BehaviourTreeUtil.RestartBehaviour(this);
        }

        public void UpdateBehaviour()
        {
            if (BehaviourTreeUtil.UpdateBehaviourNodes(this))
            {
                BehaviourInfo.RunningNode = BehaviourInfo.NextNode;
                BehaviourInfo.NextNode = -1;
            }
        }

#if BEHAVIOUR_TREE_DEV

        void Start()
        {
            Initialize();
        }

        void Update()
        {
            UpdateBehaviour();
        }
#endif
    }
}
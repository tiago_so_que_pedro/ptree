namespace PTree
{
    public enum NodeType
    {

        NONE,
        ROOT,
        COMPOSITE,
        LEAF
    }

    public enum SpecificNodeType
    {
        ROOT,
        SELECTOR_NODE,
        SEQUENCE_NODE,
        LEAF_FOLLOW_TARGET,
        PRINT,
        DETECT_TARGET
    }

    public enum NodeState
    {
        NONE,
        RUNNING,
        FAIL,
        SUCESS
    }

    public enum CompositeNodeType
    {
        SEQUENCE,
        CONDITION,
        PRINT,
        DETECT_TARGET
    }

    public enum SelectorCondition
    {
        SPOTTED_TARGET,
        REACHED_TARGET,
    }

    //LEAF NODE

    public enum LeafNodeActionType
    {
        FOLLOW_TARGET
    }
}
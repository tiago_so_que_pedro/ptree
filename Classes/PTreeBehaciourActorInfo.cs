using UnityEngine;
using System;

namespace PTree
{
    [Serializable]
    public class PTreeBehaciourActorInfo
    {
        public int RunningNode;
        public int NextNode = -1;
    }
}
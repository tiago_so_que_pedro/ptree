using UnityEngine;
using System;

namespace PTree
{
    [System.Serializable]
    public class SequenceNode : CompositeNode
    {
        public int ActionID;
        public int[] Actions;

#if UNITY_EDITOR
        public int ClickedID;
#endif
    }
}

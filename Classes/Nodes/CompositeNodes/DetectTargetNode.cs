using UnityEngine;

namespace PTree
{
    [System.Serializable]
    public class DetectTargetNode : CompositeNode
    {
        public LayerMask Target;
        public float Distance;
    }
}
using UnityEngine;

namespace PTree
{
    [System.Serializable]
    public class PrintNode : CompositeNode
    {
        public string Message;
    }
}
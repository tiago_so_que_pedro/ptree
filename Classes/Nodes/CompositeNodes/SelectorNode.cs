using UnityEngine;
using System;

namespace PTree
{
    [System.Serializable]
    public class SelectorNode : CompositeNode
    {
        public SelectorCondition Condition;
        public int TrueNode;
        public int FalseNode;
    }
}
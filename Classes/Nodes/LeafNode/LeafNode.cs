using UnityEngine;

namespace PTree
{
    [System.Serializable]
    public class LeafNode : Node
    {
        public LeafNodeActionType Type;
    }
}
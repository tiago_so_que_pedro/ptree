using UnityEngine;

namespace PTree
{
    [System.Serializable]
    public class FollowTargetNode : LeafNode
    {
        public Vector3 Destination;
    }
}
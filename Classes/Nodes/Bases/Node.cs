using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace PTree
{
    [Serializable]
    public class Node
    {
        public int ID;
        public NodeType BaseNodeType;
        public SpecificNodeType SpecificNodeType;
        public Node[] ParentNode;
        public NodeState State;

#if UNITY_EDITOR
        //EDITOR INFO
        public string NodeTitle;
        public bool Hide;
        public Rect Rect;
#endif
    }
}
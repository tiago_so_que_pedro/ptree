using UnityEngine;

namespace PTree
{
    [System.Serializable]
    public class RootNode : Node
    {
        public int ChildNode;
    }
}
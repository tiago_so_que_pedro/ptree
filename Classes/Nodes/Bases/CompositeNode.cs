using UnityEngine;

namespace PTree
{
    [System.Serializable]
    public class CompositeNode : Node
    {
        public CompositeNodeType Type;
        public int ChildNode;
    }
}
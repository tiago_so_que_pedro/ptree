﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PTree
{
    [Serializable]
    public class BehaviourTree
    {

        public string UniqueID;

        public RootNode Root;

        public Node[] Nodes;
        public CompositeNode[] CompositeNodes;
        public LeafNode[] LeafNodes;

        [Space(10f)]
        [Header("COMPOSITES")]
        public SelectorNode[] SelectorNodes;
        public SequenceNode[] SequenceNodes;
        public DetectTargetNode[] DetectTargetNode;
        public PrintNode[] PrintNodes;

        [Header("LEAFS")]
        public FollowTargetNode[] LeafFollowTargetNodes;

#if UNITY_EDITOR
        [HideInInspector]
        public Vector2 EditorScrollView;
        [HideInInspector]
        public int CreatedNodes;
        [HideInInspector]
        public Vector2 EditorZoom;
#endif
    }
}

using UnityEngine;
using PTree;

namespace PTree.Editor
{
#if UNITY_EDITOR
    public static class EditorConstants
    {

        //IDs
        public const int NODE_INSPECTOR_ID = 999999;

        //GENERIC MENUs PATHs
        public const string ADD_COMPOSITE_SELECTOR_NODE = "Add/CompositeNode/Selector";
        public const string ADD_COMPOSITE_SEQUENCE_NODE = "Add/CompositeNode/Sequence";
        public const string ADD_COMPOSITE_DETECT_TARGET_NODE = "Add/CompositeNode/Detect Target";
        public const string ADD_COMPOSITE_PRINT_NODE = "Add/CompositeNode/Print";
        public const string ADD_LEAF_GO_TO_TARGET_NODE = "Add/Leaf/Follow Target";

        public const string NODE_ACTION_ADD_SELECTOR_CONDITION = "Add Condition";
        public const string NODE_ACTION_CREATE_CONNECTION = "Add Connection";

        //WIDHT AND HEIGTH 
        public const int NODE_WIDHT = 150;
        public const int NODE_HEIGHT = 100;
        public const int NODE_CONNECTOR_BUTTON_WIDTH = 30;
        public const int NODE_CONNECTOR_BUTTON_HEIGHT = 30;
        public const int NODE_INSPECTOR_WIDTH = 200;
        public const int NODE_INSPECTOR_HEIGTH = 400;
    }
#endif
}
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;

namespace PTree.Editor
{
#if UNITY_EDITOR
    public static class DrawEditorWindowUtil
    {

        public static void DrawGenericMenu(Event e, BehaviourTreeEditorWindow window)
        {
            GenericMenu genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent(EditorConstants.ADD_COMPOSITE_SELECTOR_NODE), false, () => { EditorWindowUtil.CreateSelectorNode(e, window.BT.BehaviourTree); });
            genericMenu.AddItem(new GUIContent(EditorConstants.ADD_COMPOSITE_SEQUENCE_NODE), false, () => { EditorWindowUtil.CreateSequenceNode(e, window.BT.BehaviourTree); });
            genericMenu.AddItem(new GUIContent(EditorConstants.ADD_COMPOSITE_DETECT_TARGET_NODE), false, () => { EditorWindowUtil.CreateDetectTargetNode(e, window.BT.BehaviourTree); });
            genericMenu.AddItem(new GUIContent(EditorConstants.ADD_COMPOSITE_PRINT_NODE), false, () => { EditorWindowUtil.CreatePrintNode(e, window.BT.BehaviourTree); });
            genericMenu.AddItem(new GUIContent(EditorConstants.ADD_LEAF_GO_TO_TARGET_NODE), false, () => { EditorWindowUtil.CreateFollowTargetNode(e, window.BT.BehaviourTree); });
            genericMenu.ShowAsContext();
        }

        public static void DrawOnNodeGenericMenu(Event e, BehaviourTreeEditorWindow window)
        {
            GenericMenu nodeAction = new GenericMenu();

            if (window.ClickedNode.BaseNodeType != NodeType.LEAF)
            {
                nodeAction.AddItem(new GUIContent(EditorConstants.NODE_ACTION_CREATE_CONNECTION), false, () => { window.CreatingConnectionType = CreatingConnectionType.FROM_CONTEXT_MENU; });
            }
            if (window.ClickedNode.BaseNodeType != NodeType.ROOT)
                nodeAction.AddItem(new GUIContent("Delete"), false, () => { EditorWindowUtil.DeleteNode(window.BT, window.ClickedNode); });
            nodeAction.ShowAsContext();
        }

        public static void DrawNodes(BehaviourTreeEditorWindow window)
        {
            GUI.EndGroup();

            GUI.BeginGroup(new Rect(1, 1, window.maxSize.x, window.maxSize.y));

            window.BeginWindows();
            DrawNodeInspector(window);
            foreach (var node in window.BT.BehaviourTree.Nodes)
            {
                node.Rect = GUI.Window(node.ID, node.Rect, window.DrawNode, node.NodeTitle);
                if (window.Dragging && Event.current.button == 2)
                {
                    node.Rect.position += Event.current.delta;
                }
                else if (Event.current.type == EventType.ScrollWheel)
                {
                    window.BT.BehaviourTree.EditorZoom -= Event.current.delta * .005f;
                    window.BT.BehaviourTree.EditorZoom.x = Mathf.Clamp(window.BT.BehaviourTree.EditorZoom.x, 0.1f, 2f);
                    window.BT.BehaviourTree.EditorZoom.y = Mathf.Clamp(window.BT.BehaviourTree.EditorZoom.y, 0.1f, 2f);

                    GUIUtility.ScaleAroundPivot(window.BT.BehaviourTree.EditorZoom, Event.current.mousePosition);
                }
            }
            window.EndWindows();
        }

        public static void HandleCreatingConnectionType(Event e, BehaviourTreeEditorWindow window)
        {
            if (window.CreatingConnectionType != CreatingConnectionType.NONE)
            {

                if (EditorWindowUtil.IsCuttingConnection(e, window))
                    return;

                if (e.keyCode == KeyCode.LeftControl)
                    window.CutConnection = !window.CutConnection;


                EditorWindowUtil.SwapToDefaultNodeCreatingTransition(window);

                if (e.keyCode == KeyCode.Escape)
                {
                    window.CreatingConnectionType = CreatingConnectionType.NONE;
                    return;
                }

                Color color = Color.black;

                Vector3 startPos = new Vector3();
                Vector3 endPos = e.mousePosition;
                switch (window.CreatingConnectionType)
                {
                    case CreatingConnectionType.FROM_ROOT_NODE:
                        Node root = EditorWindowUtil.GetNode(window.BT, window.BT.BehaviourTree.Root.ID);
                        startPos = root.Rect.center;
                        startPos.y += EditorConstants.NODE_HEIGHT / 2;
                        color = Color.yellow;
                        break;
                    case CreatingConnectionType.FROM_CONTEXT_MENU:
                        startPos = window.ClickedNode.Rect.position;
                        startPos.x += window.ClickedNode.Rect.width / 2;
                        startPos.y += window.ClickedNode.Rect.height;
                        break;
                    case CreatingConnectionType.FROM_SELECTOR_FALSE:
                        startPos = window.ClickedNode.Rect.position;
                        startPos.x += window.ClickedNode.Rect.width - EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH / 2; ;
                        startPos.y += window.ClickedNode.Rect.height / 2 + EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT * 1.5f;
                        color = Color.red;
                        break;
                    case CreatingConnectionType.FROM_SELECTOR_TRUE:
                        startPos = window.ClickedNode.Rect.position;
                        startPos.x += EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH / 2;
                        startPos.y += window.ClickedNode.Rect.height / 2 + EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT * 1.5f;
                        color = Color.green;
                        break;
                    case CreatingConnectionType.FROM_SEQUENCE_NODE:
                        SequenceNode sequenceNode = EditorWindowUtil.GetSequenceNode(window.BT, window.ClickedNode.ID);
                        startPos = sequenceNode.Rect.position;
                        startPos.y += sequenceNode.Rect.height / 2 + 10 + EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT / 2;
                        startPos.x += 2 * EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH / 2 * sequenceNode.ClickedID + EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH / 2;
                        break;
                    case CreatingConnectionType.FROM_DEFAULT_COMPOSITE_NODE:
                        CompositeNode printNode = EditorWindowUtil.GetCompositeNode(window.BT, window.ClickedNode.ID);
                        startPos = printNode.Rect.position;
                        startPos.x += EditorConstants.NODE_WIDHT / 2;
                        startPos.y += EditorConstants.NODE_HEIGHT + EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT / 2;
                        break;
                }

                Vector3 startTan = startPos + Vector3.right * 50;
                Vector3 endTan = endPos + Vector3.left * 50;
                Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 2);
            }
        }

        public static void DrawConnectors(Event e, BehaviourTreeEditorWindow window)
        {
            EditorWindowUtil.RemoveInvalidConnections(window.BT);
            EditorWindowUtil.RemoveInvalidParents(window.BT);
            HandleCreatingConnectionType(e, window);


            foreach (var node in window.BT.BehaviourTree.Nodes)
            {
                switch (node.SpecificNodeType)
                {
                    case SpecificNodeType.ROOT:
                        {
                            RootNode root = window.BT.BehaviourTree.Root;


                            if (root.ChildNode != -1)
                            {
                                Vector3 startPos = new Vector3();
                                Vector3 endPos = new Vector3();
                                Color color = Color.black;

                                Node childNode = EditorWindowUtil.GetNode(window.BT, root.ChildNode);

                                startPos = root.Rect.center;
                                startPos.y += EditorConstants.NODE_HEIGHT / 2;
                                endPos = childNode.Rect.position;
                                endPos.x += EditorConstants.NODE_WIDHT / 2;

                                Vector3 startTan = startPos + Vector3.right * 0;
                                Vector3 endTan = endPos + Vector3.left * 0;

                                Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 2);
                            }
                        }
                        break;
                    case SpecificNodeType.SELECTOR_NODE:
                        {
                            SelectorNode selectorNode = EditorWindowUtil.GetSelectorNode(window.BT, node.ID);

                            if (selectorNode.TrueNode != -1)
                            {
                                Vector3 startPos = new Vector3();
                                Vector3 endPos = new Vector3();
                                Color color = Color.black;

                                Node trueNode = EditorWindowUtil.GetNode(window.BT, selectorNode.TrueNode);

                                startPos = selectorNode.Rect.position;
                                startPos.x += EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH / 2;
                                startPos.y += selectorNode.Rect.height / 2 + EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT * 1.5f;
                                endPos = trueNode.Rect.center;
                                endPos.y -= trueNode.Rect.height / 2;
                                color = Color.green;

                                Vector3 startTan = startPos + Vector3.right * 0;
                                Vector3 endTan = endPos + Vector3.left * 0;

                                Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 2);
                            }

                            if (selectorNode.FalseNode != -1)
                            {

                                Vector3 startPos = new Vector3();
                                Vector3 endPos = new Vector3();
                                Color color = Color.black;

                                Node falseNode = EditorWindowUtil.GetNode(window.BT, selectorNode.FalseNode);

                                startPos = selectorNode.Rect.position;
                                startPos.x += selectorNode.Rect.width - EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH / 2;
                                startPos.y += selectorNode.Rect.height / 2 + EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT * 1.5f;
                                endPos = falseNode.Rect.center;
                                endPos.y -= falseNode.Rect.height / 2;
                                color = Color.red;

                                Vector3 startTan = startPos + Vector3.right * 0;
                                Vector3 endTan = endPos + Vector3.left * 0;

                                Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 2);
                            }
                        }
                        break;
                    case SpecificNodeType.SEQUENCE_NODE:
                        {
                            SequenceNode sequenceNode = EditorWindowUtil.GetSequenceNode(window.BT, node.ID);
                            int index = 1;
                            foreach (var action in sequenceNode.Actions)
                            {
                                Node connectedNode = EditorWindowUtil.GetNode(window.BT, action);
                                if (EditorWindowUtil.IsValidNode(connectedNode))
                                {
                                    Vector3 startPos = new Vector3();
                                    Vector3 endPos = new Vector3();
                                    Color color = Color.black;


                                    float buttonOffset = (index == 1) ? EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH / 2 : EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH * (index - 1) + (EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH / 2);

                                    startPos = sequenceNode.Rect.position;
                                    startPos.y += sequenceNode.Rect.height / 2 + 10 + EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT / 2;
                                    startPos.x += buttonOffset;

                                    endPos = connectedNode.Rect.center;
                                    endPos.y -= connectedNode.Rect.height / 2;

                                    Vector3 startTan = startPos + Vector3.right * 0;
                                    Vector3 endTan = endPos + Vector3.left * 0;

                                    Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 2);
                                }
                                index++;
                            }
                        }
                        break;
                    case SpecificNodeType.DETECT_TARGET:
                    case SpecificNodeType.PRINT:
                        {
                            CompositeNode compositeNode = EditorWindowUtil.GetCompositeNode(window.BT, node.ID);
                            if (compositeNode.ChildNode != -1)
                            {
                                Vector3 startPos = new Vector3();
                                Vector3 endPos = new Vector3();
                                Color color = Color.black;

                                Node childNode = EditorWindowUtil.GetNode(window.BT, compositeNode.ChildNode);

                                startPos = node.Rect.position;
                                startPos.x += EditorConstants.NODE_WIDHT / 2;
                                startPos.y += EditorConstants.NODE_HEIGHT + EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT / 2;
                                endPos = childNode.Rect.position;
                                endPos.x += EditorConstants.NODE_WIDHT / 2;

                                Vector3 startTan = startPos + Vector3.right * 0;
                                Vector3 endTan = endPos + Vector3.left * 0;

                                Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 2);
                            }
                        }
                        break;
                    case SpecificNodeType.LEAF_FOLLOW_TARGET:
                        break;
                    default:
                        {
                            Debug.LogError("Implement SpecificNode of type: " + node.SpecificNodeType);
                        }
                        break;
                }
            }
        }

        public static void DrawRootNode(BehaviourTreeEditorWindow window)
        {
            GUILayout.Label("START POINT");
        }

        public static void DrawNodeHeader(BehaviourTreeEditorWindow window, Node node)
        {
            string toggleLabel = (node.Hide) ? "▼" : "▲";

            int height = (node.Hide) ? 50 : 150;

            node.Rect.size = new Vector2(EditorConstants.NODE_WIDHT, height);

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button(toggleLabel))
                {
                    node.Hide = !node.Hide;
                }
                if (GUILayout.Button("X"))
                {
                    EditorWindowUtil.DeleteNode(window.BT, node);
                }
            }
            GUILayout.EndHorizontal();
        }

        public static void DrawSelectorNode(BehaviourTreeEditorWindow window, SelectorNode selectorNode)
        {
            GUILayout.Label("Condition:");
            selectorNode.Condition = (SelectorCondition)EditorGUILayout.EnumPopup(selectorNode.Condition);

            Vector2 truePos = new Vector2();
            Vector2 falsePos = new Vector2();

            truePos.y += (selectorNode.Rect.height / 2) + EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT;

            falsePos.x = selectorNode.Rect.width - EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH;
            falsePos.y += selectorNode.Rect.height / 2 + EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT;

            Rect trueRect = new Rect(truePos, new Vector2(EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH, EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH));
            Rect falseRect = new Rect(falsePos, new Vector2(EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH, EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH));

            EditorGUILayout.BeginHorizontal();
            {
                GUILayout.Label("TRUE");
                GUILayout.FlexibleSpace();
                GUILayout.Label("FALSE");
            }
            EditorGUILayout.EndHorizontal();

            if (DrawChildConnectorButton(window, trueRect, selectorNode))
            {
                window.CreatingConnectionType = CreatingConnectionType.FROM_SELECTOR_TRUE;
            }

            if (DrawChildConnectorButton(window, falseRect, selectorNode))
            {
                window.CreatingConnectionType = CreatingConnectionType.FROM_SELECTOR_FALSE;
            }
        }

        public static void DrawSequenceNode(BehaviourTreeEditorWindow window, SequenceNode sequenceNode)
        {
            if (GUILayout.Button("Add Sequence"))
            {
                int newAction = -1;
                ArrayUtility.Add(ref sequenceNode.Actions, newAction);
            }
            GUILayout.Label("Sequences:");
            int index = 0;
            foreach (var action in sequenceNode.Actions)
            {
                Rect buttonRect = new Rect();

                float buttonOffset = (index == 0) ? 0 : EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH * index;

                buttonRect.x += 2 + buttonOffset;
                buttonRect.y += sequenceNode.Rect.height / 2 + 10;
                buttonRect.size = new Vector2(EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH, EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH);

                if (DrawChildConnectorButton(window, buttonRect, sequenceNode))
                {
                    window.CreatingConnectionType = CreatingConnectionType.FROM_SEQUENCE_NODE;
                    sequenceNode.ClickedID = index;
                }

                index++;
            }
        }

        public static void DrawDetectTargetNode(BehaviourTreeEditorWindow window, DetectTargetNode detectTargetNode)
        {
            GUILayout.Label("Target:");
            detectTargetNode.Target = EditorGUILayout.LayerField(detectTargetNode.Target);

            Rect buttonRect = new Rect
                (
                    EditorConstants.NODE_WIDHT / 2 - EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH / 2, EditorConstants.NODE_HEIGHT,
                    EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH, EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT
                );

            if (DrawChildConnectorButton(window, buttonRect, detectTargetNode))
            {
                window.CreatingConnectionType = CreatingConnectionType.FROM_DEFAULT_COMPOSITE_NODE;
            }
        }

        public static void DrawPrintNode(BehaviourTreeEditorWindow window, PrintNode printNode)
        {
            GUILayout.Label("Message:");
            printNode.Message = EditorGUILayout.TextField(printNode.Message);

            Rect buttonRect = new Rect
                (
                    EditorConstants.NODE_WIDHT / 2 - EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH / 2, EditorConstants.NODE_HEIGHT,
                    EditorConstants.NODE_CONNECTOR_BUTTON_WIDTH, EditorConstants.NODE_CONNECTOR_BUTTON_HEIGHT
                );

            if (DrawChildConnectorButton(window, buttonRect, printNode))
            {
                window.CreatingConnectionType = CreatingConnectionType.FROM_DEFAULT_COMPOSITE_NODE;
            }
        }

        private static void DrawNodeInspector(BehaviourTreeEditorWindow window)
        {
            if (window.ClickedNode.BaseNodeType != NodeType.NONE && window.ClickedNode.BaseNodeType != NodeType.ROOT)
            {
                window.NodeInspectorRect = new Rect(window.minSize.x - 50, 50, EditorConstants.NODE_INSPECTOR_WIDTH, EditorConstants.NODE_INSPECTOR_HEIGTH);
                GUI.Window(EditorConstants.NODE_INSPECTOR_ID, window.NodeInspectorRect, window.DrawNodeInspector, "Inspector");
            }
        }

        private static bool DrawChildConnectorButton(BehaviourTreeEditorWindow window, Rect buttonPos, Node parent)
        {
            if (GUI.Button(buttonPos, "⦿"))
            {
                window.FromCreatingConnectionNode = parent;
                window.ClickedNode = parent;
                return true;
            }

            return false;
        }

        private static bool DrawChildConnectorButton(BehaviourTreeEditorWindow window, Node parent)
        {

            if (GUILayout.Button("⦿"))
            {
                window.FromCreatingConnectionNode = parent;
                window.ClickedNode = parent;
                return true;
            }

            return false;
        }
    }
#endif
}
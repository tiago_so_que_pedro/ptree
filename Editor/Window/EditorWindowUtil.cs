using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace PTree.Editor
{
#if UNITY_EDITOR
    public static class EditorWindowUtil
    {
        public static EditorWindow OpenWindow(Type t)
        {

            EditorWindow window = null;

            if (t.FullName.Contains("BehaviourTreeEditorWindow"))
            {
                window = BehaviourTreeEditorWindow.OpenWindow();
            }

            return window;
        }

        public static void CreateNodeConnection(BehaviourTreeEditorWindow window)
        {
            if (window.CreatingConnectionType != CreatingConnectionType.FROM_ROOT_NODE && window.ClickedNode.BaseNodeType == NodeType.ROOT)
            {
                window.CreatingConnectionType = CreatingConnectionType.NONE;
                Debug.Log("CANOT CREATE CONNECTION TO ROOT NODE");
                return;
            }

            switch (window.CreatingConnectionType)
            {
                case CreatingConnectionType.FROM_ROOT_NODE:
                    {
                        RootNode rootNode = window.BT.BehaviourTree.Root;
                        rootNode.ChildNode = window.ClickedNode.ID;
                    }
                    break;
                case CreatingConnectionType.FROM_SELECTOR_FALSE:
                    {
                        SelectorNode selectorNode = GetSelectorNode(window.BT, window.FromCreatingConnectionNode.ID);
                        selectorNode.FalseNode = window.ClickedNode.ID;
                    }
                    break;
                case CreatingConnectionType.FROM_SELECTOR_TRUE:
                    {
                        SelectorNode selectorNode = GetSelectorNode(window.BT, window.FromCreatingConnectionNode.ID);
                        selectorNode.TrueNode = window.ClickedNode.ID;
                    }
                    break;
                case CreatingConnectionType.FROM_SEQUENCE_NODE:
                    {
                        SequenceNode sequenceNode = GetSequenceNode(window.BT, window.FromCreatingConnectionNode.ID);
                        sequenceNode.Actions[sequenceNode.ClickedID] = window.ClickedNode.ID;
                        sequenceNode.ClickedID = -1;
                    }
                    break;
                case CreatingConnectionType.FROM_DEFAULT_COMPOSITE_NODE:
                    {
                        CompositeNode composite = GetCompositeNode(window.BT, window.FromCreatingConnectionNode.ID);
                        composite.ChildNode = window.ClickedNode.ID;
                    }
                    break;
            }

            ArrayUtility.Add(ref window.ClickedNode.ParentNode, window.FromCreatingConnectionNode);
        }

        public static void SwapToDefaultNodeCreatingTransition(BehaviourTreeEditorWindow window)
        {
            if (window.ClickedNode == null)
                return;

            switch (window.ClickedNode.SpecificNodeType)
            {
                case SpecificNodeType.ROOT:
                    {
                        if (window.CreatingConnectionType == CreatingConnectionType.FROM_CONTEXT_MENU)
                        {
                            window.CreatingConnectionType = CreatingConnectionType.FROM_ROOT_NODE;
                            window.FromCreatingConnectionNode = window.BT.BehaviourTree.Root;
                        }
                    }
                    break;
                case SpecificNodeType.SELECTOR_NODE:
                    if (window.CreatingConnectionType == CreatingConnectionType.FROM_CONTEXT_MENU)
                    {
                        window.CreatingConnectionType = CreatingConnectionType.FROM_SELECTOR_TRUE;
                        window.FromCreatingConnectionNode = window.ClickedNode;
                    }
                    break;
                case SpecificNodeType.SEQUENCE_NODE:
                    {
                        SequenceNode sequenceNode = GetSequenceNode(window.BT, window.ClickedNode.ID);
                        if (window.CreatingConnectionType == CreatingConnectionType.FROM_CONTEXT_MENU)
                        {
                            if (sequenceNode.Actions.Length > 0)
                            {
                                sequenceNode.ClickedID = 0;
                                window.CreatingConnectionType = CreatingConnectionType.FROM_SEQUENCE_NODE;
                                window.FromCreatingConnectionNode = window.ClickedNode;
                            }
                            else
                            {
                                window.CreatingConnectionType = CreatingConnectionType.NONE;
                            }
                        }
                    }
                    break;
                case SpecificNodeType.DETECT_TARGET:
                case SpecificNodeType.PRINT:
                    {
                        if (window.CreatingConnectionType == CreatingConnectionType.FROM_CONTEXT_MENU)
                        {
                            window.CreatingConnectionType = CreatingConnectionType.FROM_DEFAULT_COMPOSITE_NODE;
                        }
                    }
                    break;
                case SpecificNodeType.LEAF_FOLLOW_TARGET:
                    break;
                default:
                    Debug.LogError("Implement SwapToDefaultNodeTransition for Specific Node of type " + window.ClickedNode.SpecificNodeType);
                    break;
            }
        }

        public static bool IsCuttingConnection(Event e, BehaviourTreeEditorWindow window)
        {
            if (!IsValidNode(window.FromCreatingConnectionNode) && window.CreatingConnectionType == CreatingConnectionType.NONE)
                return false;

            if (e.keyCode == KeyCode.LeftControl)
            {
                switch (window.CreatingConnectionType)
                {
                    case CreatingConnectionType.FROM_SELECTOR_FALSE:
                        {
                            SelectorNode selectorNode = GetSelectorNode(window.BT, window.FromCreatingConnectionNode.ID);
                            if (selectorNode.FalseNode == -1)
                                return false;

                            Node childNode = GetNode(window.BT, selectorNode.FalseNode);
                            RemoveParentOfNode(childNode, selectorNode);
                            selectorNode.FalseNode = -1;
                        }
                        break;
                    case CreatingConnectionType.FROM_SELECTOR_TRUE:
                        {
                            SelectorNode selectorNode = GetSelectorNode(window.BT, window.FromCreatingConnectionNode.ID);
                            if (selectorNode.TrueNode == -1)
                                return false;

                            Node childNode = GetNode(window.BT, selectorNode.FalseNode);
                            RemoveParentOfNode(childNode, selectorNode);
                            selectorNode.TrueNode = -1;
                        }
                        break;
                    case CreatingConnectionType.FROM_SEQUENCE_NODE:
                        {
                            SequenceNode sequenceNode = GetSequenceNode(window.BT, window.FromCreatingConnectionNode.ID);
                            if (sequenceNode.Actions[sequenceNode.ClickedID] == -1)
                                return false;

                            Node childNode = GetNode(window.BT, sequenceNode.Actions[sequenceNode.ClickedID]);
                            RemoveParentOfNode(childNode, sequenceNode);
                            sequenceNode.Actions[sequenceNode.ClickedID] = -1;
                        }
                        break;
                    case CreatingConnectionType.FROM_DEFAULT_COMPOSITE_NODE:
                        {
                            CompositeNode compositeNode = GetCompositeNode(window.BT, window.FromCreatingConnectionNode.ID);
                            if (compositeNode.ChildNode == -1)
                                return false;

                            Node childNode = GetNode(window.BT, compositeNode.ChildNode);
                            RemoveParentOfNode(childNode, compositeNode);
                            compositeNode.ChildNode = -1;
                        }
                        break;

                }

                return true;
            }

            return false;
        }

        public static void DeleteNode(BehaviourTreeSO BT, Node node)
        {
            for (int i = 0; i < BT.BehaviourTree.Nodes.Length; i++)
            {
                var curNode = BT.BehaviourTree.Nodes[i];
                if (curNode.ID == node.ID)
                {
                    ArrayUtility.RemoveAt(ref BT.BehaviourTree.Nodes, i);

                    if (curNode.BaseNodeType == NodeType.COMPOSITE)
                        DeleteCompositeNode(BT, curNode);
                    else if (curNode.BaseNodeType == NodeType.LEAF)
                        DeleteLeafNode(BT, curNode);

                    DeleteSpecificNode(BT, curNode);

                    break;
                }
            }
        }

        public static void DeleteCompositeNode(BehaviourTreeSO BT, Node node)
        {
            for (int i = 0; i < BT.BehaviourTree.CompositeNodes.Length; i++)
            {
                var curNode = BT.BehaviourTree.CompositeNodes[i];
                if (node.ID == curNode.ID)
                {
                    ArrayUtility.RemoveAt(ref BT.BehaviourTree.CompositeNodes, i);
                }
            }
        }

        public static void DeleteLeafNode(BehaviourTreeSO BT, Node node)
        {
            for (int i = 0; i < BT.BehaviourTree.LeafNodes.Length; i++)
            {
                var curNode = BT.BehaviourTree.LeafNodes[i];
                if (node.ID == curNode.ID)
                {
                    ArrayUtility.RemoveAt(ref BT.BehaviourTree.LeafNodes, i);
                }
            }
        }

        public static void DeleteSpecificNode(BehaviourTreeSO BT, Node node)
        {

            switch (node.SpecificNodeType)
            {
                case SpecificNodeType.SELECTOR_NODE:
                    for (int i = 0; i < BT.BehaviourTree.SelectorNodes.Length; i++)
                    {
                        var curNode = BT.BehaviourTree.SelectorNodes[i];
                        if (curNode.ID == node.ID)
                        {
                            ArrayUtility.RemoveAt(ref BT.BehaviourTree.SelectorNodes, i);
                        }
                    }
                    break;
                case SpecificNodeType.SEQUENCE_NODE:
                    {
                        for (int i = 0; i < BT.BehaviourTree.SequenceNodes.Length; i++)
                        {
                            var curNode = BT.BehaviourTree.SequenceNodes[i];
                            if (curNode.ID == node.ID)
                            {
                                ArrayUtility.RemoveAt(ref BT.BehaviourTree.SequenceNodes, i);
                            }
                        }
                    }
                    break;
                case SpecificNodeType.LEAF_FOLLOW_TARGET:
                    {
                        for (int i = 0; i < BT.BehaviourTree.LeafFollowTargetNodes.Length; i++)
                        {
                            var curNode = BT.BehaviourTree.LeafFollowTargetNodes[i];
                            if (curNode.ID == node.ID)
                            {
                                ArrayUtility.RemoveAt(ref BT.BehaviourTree.LeafFollowTargetNodes, i);
                            }
                        }
                    }
                    break;
                case SpecificNodeType.PRINT:
                    {
                        for (int i = 0; i < BT.BehaviourTree.PrintNodes.Length; i++)
                        {
                            var curNode = BT.BehaviourTree.PrintNodes[i];
                            if (curNode.ID == node.ID)
                            {
                                ArrayUtility.RemoveAt(ref BT.BehaviourTree.PrintNodes, i);
                            }
                        }
                    }
                    break;
                case SpecificNodeType.DETECT_TARGET:
                    {
                        for (int i = 0; i < BT.BehaviourTree.DetectTargetNode.Length; i++)
                        {
                            var curNode = BT.BehaviourTree.DetectTargetNode[i];
                            if (curNode.ID == node.ID)
                            {
                                ArrayUtility.RemoveAt(ref BT.BehaviourTree.DetectTargetNode, i);
                            }
                        }
                    }
                    break;
                default:
                    {
                        Debug.LogError("Implement DeleteSpecificNode for type of " + node.SpecificNodeType);
                    }
                    break;
            }
        }

        public static void RemoveInvalidParents(BehaviourTreeSO BT)
        {
            foreach (var node in BT.BehaviourTree.Nodes)
            {
                switch (node.SpecificNodeType)
                {
                    case SpecificNodeType.SELECTOR_NODE:
                        {
                            SelectorNode selectorNode = GetSelectorNode(BT, node.ID);
                            for (int i = 0; i < node.ParentNode.Length; i++)
                            {
                                if (!IsNodeExists(BT, node.ParentNode[i].ID) || !IsChildOfNode(BT, node.ParentNode[i].ID, node.ID))
                                {
                                    ArrayUtility.RemoveAt(ref selectorNode.ParentNode, i);
                                    ArrayUtility.RemoveAt(ref node.ParentNode, i);
                                }
                            }
                        }
                        break;
                    case SpecificNodeType.SEQUENCE_NODE:
                        {
                            SequenceNode sequenceNode = GetSequenceNode(BT, node.ID);
                            for (int i = 0; i < node.ParentNode.Length; i++)
                            {
                                if (!IsNodeExists(BT, node.ParentNode[i].ID) || !IsChildOfNode(BT, node.ParentNode[i].ID, node.ID))
                                {
                                    ArrayUtility.RemoveAt(ref sequenceNode.ParentNode, i);
                                    ArrayUtility.RemoveAt(ref node.ParentNode, i);
                                }
                            }
                        }
                        break;
                    case SpecificNodeType.LEAF_FOLLOW_TARGET:
                        {
                            FollowTargetNode leafFollowTargetNode = GetFollowTargetNode(BT, node.ID);

                            for (int i = 0; i < node.ParentNode.Length; i++)
                            {

                                if (!IsNodeExists(BT, node.ParentNode[i].ID) || !IsChildOfNode(BT, node.ParentNode[i].ID, node.ID))
                                {
                                    ArrayUtility.RemoveAt(ref leafFollowTargetNode.ParentNode, i);
                                    ArrayUtility.RemoveAt(ref node.ParentNode, i);
                                }
                            }
                        }
                        break;
                    case SpecificNodeType.PRINT:
                        {
                            PrintNode printNode = GetPrintNode(BT, node.ID);
                            for (int i = 0; i < node.ParentNode.Length; i++)
                            {
                                if (!IsNodeExists(BT, node.ParentNode[i].ID) || !IsChildOfNode(BT, node.ParentNode[i].ID, node.ID))
                                {
                                    ArrayUtility.RemoveAt(ref printNode.ParentNode, i);
                                    ArrayUtility.RemoveAt(ref node.ParentNode, i);
                                }
                            }
                        }
                        break;
                    case SpecificNodeType.DETECT_TARGET:
                        {
                            DetectTargetNode detectTargetNode = GetDetectTargetNode(BT, node.ID);
                            for (int i = 0; i < node.ParentNode.Length; i++)
                            {
                                if (!IsNodeExists(BT, node.ParentNode[i].ID) || !IsChildOfNode(BT, node.ParentNode[i].ID, node.ID))
                                {
                                    ArrayUtility.RemoveAt(ref detectTargetNode.ParentNode, i);
                                    ArrayUtility.RemoveAt(ref node.ParentNode, i);
                                }
                            }
                        }
                        break;
                    case SpecificNodeType.ROOT:
                        {
                            continue;
                        }
                    default:
                        Debug.LogError("Implement RemoveInvalidParents for SpecificNode of type: " + node.SpecificNodeType);
                        break;
                }
            }
        }

        public static void RemoveParentOfNode(Node child, Node parentToRemove)
        {
            for (int i = 0; i < child.ParentNode.Length; i++)
            {
                if (child.ParentNode[i].ID == parentToRemove.ID)
                {
                    ArrayUtility.RemoveAt(ref child.ParentNode, i);
                }
            }
        }

        public static void RemoveInvalidConnections(BehaviourTreeSO BT)
        {
            for (int i = 0; i < BT.BehaviourTree.Nodes.Length; i++)
            {
                var curNode = BT.BehaviourTree.Nodes[i];
                switch (curNode.SpecificNodeType)
                {
                    case SpecificNodeType.ROOT:
                        {
                            Node childNode = GetNode(BT, BT.BehaviourTree.Root.ChildNode);
                            if (childNode == null)
                                BT.BehaviourTree.Root.ChildNode = -1;
                        }
                        break;
                    case SpecificNodeType.SELECTOR_NODE:
                        {
                            SelectorNode selectorNode = GetSelectorNode(BT, curNode.ID);
                            if (selectorNode.FalseNode != -1)
                            {
                                Node falseNode = GetNode(BT, selectorNode.FalseNode);
                                if (falseNode == null)
                                    selectorNode.FalseNode = -1;
                            }
                            if (selectorNode.TrueNode != -1)
                            {
                                Node trueNode = GetNode(BT, selectorNode.TrueNode);
                                if (trueNode == null)
                                    selectorNode.TrueNode = -1;
                            }
                        }
                        break;
                    case SpecificNodeType.SEQUENCE_NODE:
                        {
                            SequenceNode sequenceNode = GetSequenceNode(BT, curNode.ID);
                            for (int j = 0; j < sequenceNode.Actions.Length; j++)
                            {
                                Node actionNode = GetNode(BT, sequenceNode.Actions[j]);
                                if (sequenceNode.Actions[j] == -1)
                                    continue;

                                if (actionNode == null)
                                {
                                    sequenceNode.Actions[j] = -1;
                                }
                            }
                        }
                        break;
                    case SpecificNodeType.DETECT_TARGET:
                    case SpecificNodeType.PRINT:
                        {
                            CompositeNode composite = GetCompositeNode(BT, curNode.ID);
                            Node childNode = GetNode(BT, composite.ChildNode);
                            if (childNode == null)
                            {
                                composite.ChildNode = -1;
                            }
                        }
                        break;
                    case SpecificNodeType.LEAF_FOLLOW_TARGET:
                        break;
                    default:
                        Debug.LogError("Implement SpecificNode for type of" + curNode.SpecificNodeType);
                        break;
                }
            }
        }

        public static Node GetNode(BehaviourTreeSO BT, int id)
        {
            for (int i = 0; i < BT.BehaviourTree.Nodes.Length; i++)
            {
                var curNode = BT.BehaviourTree.Nodes[i];
                if (curNode.ID == id)
                    return curNode;
            }
            return null;
        }

        public static SelectorNode GetSelectorNode(BehaviourTreeSO BT, int id)
        {
            for (int i = 0; i < BT.BehaviourTree.SelectorNodes.Length; i++)
            {
                var selectorNode = BT.BehaviourTree.SelectorNodes[i];
                if (selectorNode.ID == id)
                    return selectorNode;
            }

            return null;
        }

        public static SequenceNode GetSequenceNode(BehaviourTreeSO BT, int id)
        {
            for (int i = 0; i < BT.BehaviourTree.SequenceNodes.Length; i++)
            {
                var sequenceNode = BT.BehaviourTree.SequenceNodes[i];
                if (sequenceNode.ID == id)
                    return sequenceNode;
            }

            return null;
        }

        public static DetectTargetNode GetDetectTargetNode(BehaviourTreeSO BT, int id)
        {
            for (int i = 0; i < BT.BehaviourTree.DetectTargetNode.Length; i++)
            {
                var detecTargetNode = BT.BehaviourTree.DetectTargetNode[i];
                if (detecTargetNode.ID == id)
                    return detecTargetNode;
            }

            return null;
        }

        public static PrintNode GetPrintNode(BehaviourTreeSO BT, int id)
        {
            for (int i = 0; i < BT.BehaviourTree.PrintNodes.Length; i++)
            {
                var printNode = BT.BehaviourTree.PrintNodes[i];
                if (printNode.ID == id)
                    return printNode;
            }
            return null;
        }

        public static FollowTargetNode GetFollowTargetNode(BehaviourTreeSO BT, int id)
        {
            for (int i = 0; i < BT.BehaviourTree.LeafFollowTargetNodes.Length; i++)
            {
                var followTargetNode = BT.BehaviourTree.LeafFollowTargetNodes[i];
                if (followTargetNode.ID == id)
                    return followTargetNode;
            }

            return null;
        }

        public static bool RootExists(BehaviourTreeSO BT)
        {
            if (BT.BehaviourTree.Root.SpecificNodeType == SpecificNodeType.ROOT && BT.BehaviourTree.Root.BaseNodeType == NodeType.ROOT)
                return true;

            return false;
        }

        public static bool IsValidNode(Node node = null)
        {
            if (node == null || node.BaseNodeType == NodeType.NONE)
                return false;

            return true;
        }

        public static bool IsNodeExists(BehaviourTreeSO BT, int id)
        {
            Node node = GetNode(BT, id);
            return node != null;
        }

        public static bool IsChildOfNode(BehaviourTreeSO BT, int parentID, int childID)
        {
            if (!IsNodeExists(BT, parentID) || !IsNodeExists(BT, childID))
                return false;

            Node parentNode = GetNode(BT, parentID);

            switch (parentNode.SpecificNodeType)
            {
                case SpecificNodeType.SELECTOR_NODE:
                    {
                        SelectorNode selectorNode = GetSelectorNode(BT, parentNode.ID);
                        if (selectorNode.TrueNode == childID || selectorNode.FalseNode == childID)
                            return true;
                    }
                    break;
                case SpecificNodeType.SEQUENCE_NODE:
                    {
                        SequenceNode sequenceNode = GetSequenceNode(BT, parentNode.ID);
                        foreach (var action in sequenceNode.Actions)
                        {
                            if (action == childID)
                                return true;
                        }
                    }
                    break;
                case SpecificNodeType.PRINT:
                    {
                        PrintNode printNode = GetPrintNode(BT, parentNode.ID);
                        if (printNode.ChildNode == childID)
                        {
                            return true;
                        }
                    }
                    break;
                case SpecificNodeType.DETECT_TARGET:
                    {
                        DetectTargetNode detectTargetNode = GetDetectTargetNode(BT, parentNode.ID);
                        if (detectTargetNode.ChildNode == childID)
                        {
                            return true;
                        }
                    }
                    break;
                case SpecificNodeType.ROOT:
                    break;
                default:
                    Debug.LogError("Implement IsNodeConnectedWith for SpecificType of type " + parentNode.SpecificNodeType);
                    break;
            }

            return false;
        }

        public static CompositeNode GetCompositeNode(BehaviourTreeSO BT, int id)
        {
            for (int i = 0; i < BT.BehaviourTree.CompositeNodes.Length; i++)
            {
                var curNode = BT.BehaviourTree.CompositeNodes[i];
                if (curNode.ID == id)
                    return curNode;
            }

            return null;
        }

        public static LeafNode GetLeafNode(BehaviourTreeSO BT, int id)
        {
            for (int i = 0; i < BT.BehaviourTree.LeafNodes.Length; i++)
            {
                var curNode = BT.BehaviourTree.LeafNodes[i];
                if (curNode.ID == id)
                    return curNode;
            }

            return null;
        }

        public static SelectorNode CreateSelectorNode(Event e, BehaviourTree behaviourTree)
        {
            SelectorNode selectorNode = new SelectorNode();
            InitializeBaseNode(e, behaviourTree, selectorNode, NodeType.COMPOSITE, SpecificNodeType.SELECTOR_NODE);
            InitializeCompositeNode(behaviourTree, selectorNode, CompositeNodeType.CONDITION);
            selectorNode.TrueNode = -1;
            selectorNode.FalseNode = -1;
            selectorNode.NodeTitle = "Compsite: Selector";

            ArrayUtility.Add(ref behaviourTree.Nodes, selectorNode);
            ArrayUtility.Add(ref behaviourTree.SelectorNodes, selectorNode);
            return selectorNode;
        }

        public static DetectTargetNode CreateDetectTargetNode(Event e, BehaviourTree behaviourTree)
        {
            DetectTargetNode detectTarget = new DetectTargetNode();
            InitializeBaseNode(e, behaviourTree, detectTarget, NodeType.COMPOSITE, SpecificNodeType.DETECT_TARGET);
            InitializeCompositeNode(behaviourTree, detectTarget, CompositeNodeType.DETECT_TARGET);
            detectTarget.NodeTitle = "Composite: Detect Target";

            ArrayUtility.Add(ref behaviourTree.Nodes, detectTarget);
            ArrayUtility.Add(ref behaviourTree.DetectTargetNode, detectTarget);
            return detectTarget;
        }

        public static PrintNode CreatePrintNode(Event e, BehaviourTree behaviourTree)
        {
            PrintNode printNode = new PrintNode();
            InitializeBaseNode(e, behaviourTree, printNode, NodeType.COMPOSITE, SpecificNodeType.PRINT);
            InitializeCompositeNode(behaviourTree, printNode, CompositeNodeType.PRINT);
            printNode.NodeTitle = "Composite: Print";

            ArrayUtility.Add(ref behaviourTree.Nodes, printNode);
            ArrayUtility.Add(ref behaviourTree.PrintNodes, printNode);
            return printNode;
        }

        public static SequenceNode CreateSequenceNode(Event e, BehaviourTree behaviourTree)
        {
            SequenceNode sequenceNode = new SequenceNode();
            InitializeBaseNode(e, behaviourTree, sequenceNode, NodeType.COMPOSITE, SpecificNodeType.SEQUENCE_NODE);
            InitializeCompositeNode(behaviourTree, sequenceNode, CompositeNodeType.SEQUENCE);
            sequenceNode.Actions = new int[0];
            sequenceNode.NodeTitle = "Composite: Sequence";

            ArrayUtility.Add(ref behaviourTree.Nodes, sequenceNode);
            ArrayUtility.Add(ref behaviourTree.SequenceNodes, sequenceNode);
            return sequenceNode;
        }

        public static FollowTargetNode CreateFollowTargetNode(Event e, BehaviourTree behaviourTree)
        {
            FollowTargetNode followTargetNode = new FollowTargetNode();
            InitializeBaseNode(e, behaviourTree, followTargetNode, NodeType.LEAF, SpecificNodeType.LEAF_FOLLOW_TARGET);
            InitializeLeafNode(behaviourTree, followTargetNode, LeafNodeActionType.FOLLOW_TARGET);
            followTargetNode.NodeTitle = "Leaf: Follow Target";

            ArrayUtility.Add(ref behaviourTree.Nodes, followTargetNode);
            ArrayUtility.Add(ref behaviourTree.LeafFollowTargetNodes, followTargetNode);
            return followTargetNode;
        }

        private static void InitializeCompositeNode(BehaviourTree behaviourTree, CompositeNode node, CompositeNodeType compositeNodeType)
        {
            node.Type = compositeNodeType;
            node.ChildNode = -1;
            ArrayUtility.Add(ref behaviourTree.CompositeNodes, node);
        }

        private static void InitializeLeafNode(BehaviourTree behaviourTree, LeafNode node, LeafNodeActionType leafNodeActionType)
        {
            node.Type = leafNodeActionType;
            ArrayUtility.Add(ref behaviourTree.LeafNodes, node);
        }

        private static void InitializeBaseNode(Event e, BehaviourTree behaviourTree, Node node, NodeType baseNodeType, SpecificNodeType specificNode)
        {
            node.ID = behaviourTree.CreatedNodes;
            node.BaseNodeType = baseNodeType;
            node.SpecificNodeType = specificNode;
            node.ParentNode = new Node[0];
            node.State = NodeState.NONE;
            node.Rect = new Rect(e.mousePosition, new Vector2(EditorConstants.NODE_WIDHT, EditorConstants.NODE_HEIGHT));
            behaviourTree.CreatedNodes++;
        }
    }
#endif
}
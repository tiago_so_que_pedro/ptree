﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace PTree.Editor
{
#if UNITY_EDITOR

    public class BehaviourTreeEditorWindow : EditorWindow
    {
        public BehaviourTreeSO BT;

        public bool Dragging;

        public Node ClickedNode;
        public Rect NodeInspectorRect;

        //Connection Handlers
        public bool CutConnection = false;
        public Node FromCreatingConnectionNode;
        public CreatingConnectionType CreatingConnectionType;

        public static Event e;

        public static BehaviourTreeEditorWindow OpenWindow()
        {
            BehaviourTreeEditorWindow window = (BehaviourTreeEditorWindow)EditorWindow.GetWindow(typeof(BehaviourTreeEditorWindow));
            return window;
        }

        public static void InitializeInvalideArrays(BehaviourTreeEditorWindow window)
        {
            BehaviourTree behaviourTree = window.BT.BehaviourTree;

            string outputLabel = "Initializing Invalid Arrays";

            if (behaviourTree.Nodes == null)
            {
                behaviourTree.Nodes = new Node[0];
                outputLabel += "\n Node";
            }

            if (behaviourTree.CompositeNodes == null)
            {
                behaviourTree.CompositeNodes = new CompositeNode[0];
                outputLabel += "\n CompositeNode";
            }

            if (behaviourTree.LeafNodes == null)
            {
                behaviourTree.LeafNodes = new LeafNode[0];
                outputLabel += "\n LeafNodes";

            }

            if (behaviourTree.SelectorNodes == null)
            {
                behaviourTree.SelectorNodes = new SelectorNode[0];
                outputLabel += "\n SelectorNode";
            }

            if (behaviourTree.SequenceNodes == null)
            {
                behaviourTree.SequenceNodes = new SequenceNode[0];
                outputLabel += "\n SequenceNode";
            }

            if (behaviourTree.DetectTargetNode == null)
            {
                behaviourTree.DetectTargetNode = new DetectTargetNode[0];
                outputLabel += "\n DetectTargetNode";
            }

            if (behaviourTree.PrintNodes == null)
            {
                behaviourTree.PrintNodes = new PrintNode[0];
                outputLabel += "\n PrintNoed";
            }

            if (behaviourTree.LeafFollowTargetNodes == null)
            {
                behaviourTree.LeafFollowTargetNodes = new FollowTargetNode[0];
                outputLabel += "\n LeafFollowTargetNode";
            }

            Debug.Log(outputLabel);
        }

        void OnGUI()
        {
            if (BT == null)
            {
                GUILayout.Label("BehaviourTree is null");
                return;
            }

            if (EditorWindowUtil.IsValidNode(ClickedNode))
                GUILayout.Label("ClickedNode: " + ClickedNode.ID);

            GUILayout.Label("CreatingConnectionType: " + CreatingConnectionType);

            if (EditorWindowUtil.IsValidNode(FromCreatingConnectionNode))
                GUILayout.Label("FromCreatingConnectionNode: " + FromCreatingConnectionNode.ID);

            GUILayout.Label("CutConnection: " + CutConnection);

            InitializeRootNode();

            e = Event.current;
            UserInput(e);

            Matrix4x4 oldMatrix = GUI.matrix;

            if (BT.BehaviourTree.EditorZoom == Vector2.zero)
                BT.BehaviourTree.EditorZoom = Vector2.one;

            Matrix4x4 Scale = Matrix4x4.Scale(new Vector3(BT.BehaviourTree.EditorZoom.y, BT.BehaviourTree.EditorZoom.y, BT.BehaviourTree.EditorZoom.y));
            GUI.matrix = Scale;

            DrawEditorWindowUtil.DrawNodes(this);
            DrawEditorWindowUtil.DrawConnectors(e, this);
            Repaint();
            EditorUtility.SetDirty(BT);
        }

        void UserInput(Event e)
        {
            if (!e.isMouse)
                return;

            if (e.button == 1)
            {
                if (!ClickedOnNode(e))
                    DrawEditorWindowUtil.DrawGenericMenu(e, this);
                else
                    DrawEditorWindowUtil.DrawOnNodeGenericMenu(e, this);
            }
            else if (e.button == 0)
            {
                if (ClickedOnNode(e) && FromCreatingConnectionNode != ClickedNode && CreatingConnectionType != CreatingConnectionType.NONE)
                {
                    EditorWindowUtil.CreateNodeConnection(this);

                    FromCreatingConnectionNode = null;
                    CreatingConnectionType = CreatingConnectionType.NONE;
                }
            }

            Dragging = e.delta != Vector2.zero;
        }

        bool ClickedOnNode(Event e, bool changeClickedNode = true)
        {
            for (int i = 0; i < BT.BehaviourTree.Nodes.Length; i++)
            {
                var curNode = BT.BehaviourTree.Nodes[i];
                if (curNode.Rect.Contains(e.mousePosition))
                {
                    if (changeClickedNode)
                        ClickedNode = curNode;
                    return true;
                }
            }
            return false;
        }

        #region DRAW NODES

        public void InitializeRootNode()
        {
            if (!EditorWindowUtil.RootExists(BT))
            {
                if (BT.BehaviourTree.Nodes == null)
                    BT.BehaviourTree.Nodes = new Node[0];

                BT.BehaviourTree.Root.ID = 1000;
                BT.BehaviourTree.Root.BaseNodeType = NodeType.ROOT;
                BT.BehaviourTree.Root.SpecificNodeType = SpecificNodeType.ROOT;
                BT.BehaviourTree.Root.Rect = new Rect(20, 20, EditorConstants.NODE_WIDHT, EditorConstants.NODE_HEIGHT);
                BT.BehaviourTree.Root.ChildNode = -1;
                ArrayUtility.Add(ref BT.BehaviourTree.Nodes, BT.BehaviourTree.Root);
            }
        }

        public void DrawRoot(int id)
        {
            DrawEditorWindowUtil.DrawRootNode(this);
            GUI.DragWindow();
        }

        public void DrawNodeInspector(int id)
        {
            switch (ClickedNode.SpecificNodeType)
            {
                default:
                    Debug.LogError("Implement SpecificNode Type for: " + ClickedNode.SpecificNodeType);
                    break;
            }
        }

        public void DrawNode(int id)
        {
            Node node = EditorWindowUtil.GetNode(BT, id);

            if (node == null)
                return;

            if (node.SpecificNodeType != SpecificNodeType.ROOT)
                DrawEditorWindowUtil.DrawNodeHeader(this, node);

            switch (node.SpecificNodeType)
            {
                case SpecificNodeType.ROOT:
                    {
                        DrawEditorWindowUtil.DrawRootNode(this);
                        Node rootNode = EditorWindowUtil.GetNode(BT, BT.BehaviourTree.Root.ID);
                        BT.BehaviourTree.Root.Rect = rootNode.Rect;
                    }
                    break;
                case SpecificNodeType.SELECTOR_NODE:
                    {
                        SelectorNode selectorNode = EditorWindowUtil.GetSelectorNode(BT, id);

                        if (selectorNode != null)
                        {
                            selectorNode.Rect = node.Rect;
                            DrawEditorWindowUtil.DrawSelectorNode(this, selectorNode);

                        }
                    }
                    break;
                case SpecificNodeType.SEQUENCE_NODE:
                    {
                        SequenceNode sequenceNode = EditorWindowUtil.GetSequenceNode(BT, id);
                        if (sequenceNode != null)
                        {
                            sequenceNode.Rect = node.Rect;
                            DrawEditorWindowUtil.DrawSequenceNode(this, sequenceNode);
                        }
                    }
                    break;
                case SpecificNodeType.DETECT_TARGET:
                    {
                        DetectTargetNode detectNode = EditorWindowUtil.GetDetectTargetNode(BT, id);

                        if (detectNode != null)
                        {
                            detectNode.Rect = node.Rect;
                            DrawEditorWindowUtil.DrawDetectTargetNode(this, detectNode);
                        }
                    }
                    break;
                case SpecificNodeType.PRINT:
                    {
                        PrintNode printNode = EditorWindowUtil.GetPrintNode(BT, id);
                        if (printNode != null)
                        {

                            printNode.Rect = node.Rect;
                            DrawEditorWindowUtil.DrawPrintNode(this, printNode);
                        }
                    }
                    break;
                case SpecificNodeType.LEAF_FOLLOW_TARGET:
                    break;
                default:
                    {
                        Debug.LogError("Implement SpecificNode of type" + node.SpecificNodeType);
                    }
                    break;
            }
            if (Event.current.button != 2)
                GUI.DragWindow();
        }
        #endregion

    }
#endif
}

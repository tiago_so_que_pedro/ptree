using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;

namespace PTree.Editor
{
#if UNITY_EDITOR
    public static class EditorStyle
    {
        public static GUIStyle h1(bool bold = true)
        {
            GUIStyle style = new GUIStyle();
            style.fontSize = 12;
            if (bold)
                style.fontStyle = FontStyle.Bold;
            else
                style.fontStyle = FontStyle.Normal;

            return style;
        }

        public static GUIStyle h2(bool bold = true)
        {
            GUIStyle style = new GUIStyle();
            style.fontSize = 10;
            if (bold)
                style.fontStyle = FontStyle.Bold;
            else
                style.fontStyle = FontStyle.Normal;

            return style;
        }
    }
#endif
}
using UnityEngine;
using System;
using UnityEditor;
using PTree;

namespace PTree.Editor
{
#if UNITY_EDITOR
    [CustomEditor(typeof(BehaviourTreeSO))]
    public class BehaviourTreeSOCustomInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            BehaviourTreeSO behaviourTree = (BehaviourTreeSO)target;

            if (behaviourTree == null)
                return;

            BehaviourTree tree = behaviourTree.BehaviourTree;

            GUILayout.Label("Unique ID: " + tree.UniqueID, EditorStyle.h1());
            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Generate New BT"))
                {
                    behaviourTree.InitializeNewBehaviourTree();
                    AssetDatabase.SaveAssets();
                }
                if (GUILayout.Button("Edit"))
                {
                    BehaviourTreeEditorWindow window = (BehaviourTreeEditorWindow)EditorWindowUtil.OpenWindow(typeof(BehaviourTreeEditorWindow));
                    window.BT = behaviourTree;
                    BehaviourTreeEditorWindow.InitializeInvalideArrays(window);
                }
            }
            EditorGUILayout.EndHorizontal();
            if (behaviourTree.BehaviourTree.Nodes == null)
                behaviourTree.BehaviourTree.Nodes = new Node[0];

            if (behaviourTree.BehaviourTree.Nodes.Length > 0)
                GUILayout.Label("NODES:");

            for (int i = 0; i < behaviourTree.BehaviourTree.Nodes.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    string name = behaviourTree.BehaviourTree.Nodes[i].GetType().ToString();
                    GUILayout.Label(name);
                    if (GUILayout.Button("X", GUILayout.Width(50)))
                    {
                        ArrayUtility.RemoveAt(ref behaviourTree.BehaviourTree.Nodes, i);
                        i--;
                    }
                }
                EditorGUILayout.EndHorizontal();

            }


            base.OnInspectorGUI();
        }
    }
#endif
}
#if UNITY_EDITOR
namespace PTree.Editor
{
    public enum CreatingConnectionType
    {
        NONE,
        FROM_ROOT_NODE,
        FROM_CONTEXT_MENU,
        FROM_SELECTOR_FALSE,
        FROM_SELECTOR_TRUE,
        FROM_SEQUENCE_NODE,
        FROM_DEFAULT_COMPOSITE_NODE,
    }
}
#endif
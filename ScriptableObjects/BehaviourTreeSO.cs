﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace PTree
{
    [CreateAssetMenu(menuName = "PTree/BehaviourTree", fileName = "BehaviourTree")]
    public class BehaviourTreeSO : ScriptableObject
    {
        public BehaviourTree BehaviourTree;

#if UNITY_EDITOR

        void OnValidate()
        {

        }

        public void InitializeNewBehaviourTree()
        {
            BehaviourTree = new BehaviourTree();

            BehaviourTree.UniqueID = Guid.NewGuid().ToString();
            BehaviourTree.Nodes = new Node[0];
            BehaviourTree.LeafNodes = new LeafNode[0];
            BehaviourTree.CompositeNodes = new CompositeNode[0];

            //INITIALIZING SPECIFIC NODES
            BehaviourTree.SequenceNodes = new SequenceNode[0];
            BehaviourTree.SelectorNodes = new SelectorNode[0];
            BehaviourTree.LeafFollowTargetNodes = new FollowTargetNode[0];
            BehaviourTree.PrintNodes = new PrintNode[0];
            BehaviourTree.DetectTargetNode = new DetectTargetNode[0];

            // Debug.Log("Generated a new BehaviourTree with GUID: " + BehaviourTree.UniqueID);
            Debug.Log("Generated a new BehaviourTree with GUID: " + BehaviourTree.UniqueID);
        }

#endif
    }
}
